let app = require('express')(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    userController = require('./controller/user')
;
const authenticator   = require('./plugin/authenticator');
const log_handler = require('./plugin/log_handler');

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {

    if (!socket.handshake.query.token) {
        log_handler.log("socket.connection.unauthorized", socket.handshake, null, {'message': 'Undefined Token'});
        socket.disconnect();
    } else {
        let token = socket.handshake.query.token;
        authenticator.verify(token, function (result) {
            if (result && result.id) {
                log_handler.log("socket.connection.authorized", socket.handshake, null, {'message': 'Success', 'data': result});
            } else {
                log_handler.log("socket.connection.unauthorized", socket.handshake, null, {'message': 'Invalid Token'});
                socket.disconnect();
            }
        });
    }

    io.on('disconnect', function (data) {
        log_handler.log("io.disconnect", socket.handshake, null, {'message': 'Success'});
        socket.disconnect();
    });

    socket.on('updateLocation', function (message) {
        log_handler.log("socket.updateLocation", socket.handshake, message, null);
        console.log('updateLocation - ' + JSON.stringify(message));
        userController.updateLocation(message);
    });

    socket.on('disconnect', function (data) {
        console.log('Got disconnect!');
        log_handler.log("socket.disconnect", socket.handshake, null, {'message': 'Success'});
        socket.disconnect();
    });
});


http.listen(process.env.port || 3000, function () {
    console.log('listening on *:' + process.env.port || 3000);
});