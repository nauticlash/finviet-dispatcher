ALTER TABLE `setting_master`
ADD COLUMN `order_pending_time` int(0) NULL DEFAULT 0 AFTER `ios_heremap_app_code`,
ADD COLUMN `alert_email_notification` tinyint(1) NULL DEFAULT 0 AFTER `order_pending_time`,
ADD COLUMN `alert_email_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL AFTER `alert_email_notification`,
ADD COLUMN `rider_pickup_velocity` int(0) NULL DEFAULT 0 AFTER `alert_email_list`,
ADD COLUMN `rider_dropoff_velocity` int(0) NULL DEFAULT 0 AFTER `rider_pickup_velocity`;

ALTER TABLE `order_master`
ADD COLUMN `alert_pending` tinyint(1) NULL DEFAULT 0 AFTER `cancel_reason`,
ADD COLUMN `alert_pickup` tinyint(1) NULL DEFAULT 0 AFTER `alert_pending`,
ADD COLUMN `alert_dropoff` tinyint(1) NULL DEFAULT 0 AFTER `alert_pickup`,
ADD COLUMN `last_alert_pending` timestamp(0) NULL AFTER `alert_dropoff`,
ADD COLUMN `last_alert_pickup` timestamp(0) NULL AFTER `last_alert_pending`,
ADD COLUMN `last_alert_dropoff` timestamp(0) NULL AFTER `last_alert_pickup`;

ALTER TABLE `setting_master`
ADD COLUMN `order_pending_retry` tinyint(2) UNSIGNED NULL DEFAULT 0 AFTER `order_pending_time`;