module.exports = {
    apps: [{
        name: "finviet-job",
        script: "jobs.js",
        node_args: ["--max-old-space-size=5120"],
        autorestart: true,
        timestamp: "YYYY-MM-DD HH:mm Z",
        exec_mode: "fork",
        env: {
            "ENV": "dev",
            "port": 7777
        }
    }, {
        name: "finviet-socket",
        script: "socket.js",
        node_args: ["--max-old-space-size=5120"],
        autorestart: true,
        timestamp: "YYYY-MM-DD HH:mm Z",
        exec_mode: "fork",
        env: {
            "ENV": "dev",
            "port": 7778
        }
    }, {
        name: "finviet-queue",
        script: "queue.js",
        node_args: ["--max-old-space-size=5120"],
        autorestart: true,
        timestamp: "YYYY-MM-DD HH:mm Z",
        exec_mode: "fork",
        env: {
            "ENV": "dev",
            "port": 7779
        }
    }
    ]
}