'use strict';
let mysqlConnector = require('../plugin/mysql_connector'),
    mongoConnector = require("../plugin/mongo_connector"),
    redisConnector = require('../plugin/redis_connector'),
    moment = require('moment')
;

exports.updateLocation = function(data) {
    mongoConnector.saveLocation(data);
    console.log("TIME DIFF " + moment().diff(moment(data.timestamp), 'seconds'), 'seconds');
    if (data.timestamp && moment().diff(moment(data.timestamp), 'seconds') <= 10) {
        mysqlConnector.saveLocation(data);
        redisConnector.saveLocation(data);
    }
}