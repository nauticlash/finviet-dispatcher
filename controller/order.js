'use strict';
let schedule = require('node-schedule'),
    mysqlConnector = require('../plugin/mysql_connector'),
    email = require('../plugin/email'),
    notification = require('../plugin/notification_connector')
;
const env = process.env.ENV || 'dev',
    config = require('../config/'+env+'/config'),
    request = require('request');

class OrderController {
    runWithFormat(format, callback, params) {
        console.log('1. Job Start: '+ new Date());
        var j = schedule.scheduleJob(format, function(){
            if (typeof callback === "function") {
                callback(params);
            }
        });
    };

    pendingOrders() {
        console.log('Job Pending Order at: '+ new Date());
        let settings = mysqlConnector.getAlertSettings();
        settings.then(async function (setting) {
            let result = await mysqlConnector.getExpiredPendingOrder(setting);
            let overExpired = await mysqlConnector.getOverExpiredPendingOrder(setting);
            if (result.length > 0) {
                console.log(result.length + " Pending Orders");
                console.log(result);
                let ids = result.map(function(item){ return item.id });
                mysqlConnector.setAlertPending(ids);
                if (setting.alert_email_notification && setting.alert_email_list !== '') {
                    let data = {
                        to: setting.alert_email_list,
                        message: 'Có ' + result.length + ' đơn hàng đang chờ lái xe nhận. Vui lòng kiểm tra trên hệ thống',
                        subject: 'Đơn hàng chưa có lái xe nhận'
                    };
                    email.send(data, function (info) {
                        console.log(info);
                    });
                }
            } else {
                console.log("No Pending Order");
            }
            console.log(overExpired);
            if (overExpired.length > 0) {
                overExpired.forEach(function(element) {
                    let requestData = {
                        service: "admin_cancel_order",
                        auth: {
                            token: "finviet",
                            id: 1,
                        },
                        request: {"orderId": element.id, "reason": "Hệ thống tự động hủy do quá lâu chưa có lái xe nhận"},
                    };
                    request.post({
                        url: config.api,
                        body: JSON.stringify(requestData)
                    }, function(error, response, body){
                    });
                });
            }
        });
    };

    pendingPickups() {
        console.log('Job Pending Pickup at: '+ new Date());
        let settings = mysqlConnector.getAlertSettings()
        settings.then(async function (setting) {
            let result = await mysqlConnector.getExpiredPendingPickup(setting);
            if (result.length > 0) {
                console.log(result.length + " Pending Pickups");
                console.log(result);
                let ids = result.map(function(item){ return item.id });
                mysqlConnector.setAlertPickup(ids);
                if (setting.alert_email_notification && setting.alert_email_list !== '') {
                    let data = {
                        to: setting.alert_email_list,
                        message: 'Có ' + result.length + ' đơn hàng lái xe chưa lấy hàng. Vui lòng kiểm tra trên hệ thống',
                        subject: 'Đơn hàng lái xe chưa lấy hàng'
                    };
                    email.send(data, function (info) {
                        console.log(info);
                    });
                }
            } else {
                console.log("No Pending Pickup");
            }
        });
    };

    pendingDropoffs() {
        console.log('Job Pending Dropoff at: '+ new Date());
        let settings = mysqlConnector.getAlertSettings();
        settings.then(async function (setting) {
            let result = await mysqlConnector.getExpiredPendingDropoff(setting);
            if (result.length > 0) {
                console.log(result.length + " Pending Dropoffs");
                console.log(result);
                let ids = result.map(function(item){ return item.id });
                mysqlConnector.setAlertDropoff(ids);
                if (setting.alert_email_notification && setting.alert_email_list !== '') {
                    let data = {
                        to: setting.alert_email_list,
                        message: 'Có ' + result.length + ' đơn hàng lái xe chưa giao hàng. Vui lòng kiểm tra trên hệ thống',
                        subject: 'Đơn hàng lái xe chưa giao hàng'
                    };
                    email.send(data, function (info) {
                        console.log(info);
                    });
                }
            } else {
                console.log("No Pending Dropoff");
            }
        });
    };

    newOrderNotification(orderDetail) {
        let obj = this;
        console.log('New Order Detail -- ' + JSON.stringify(orderDetail) + ' -- at: ' + new Date());
        if (orderDetail.token && orderDetail.user_id && orderDetail.order_id && orderDetail.lat && orderDetail.lng && orderDetail.radius) {

            let data = {
                'service': 'placeorderNotification',
                'auth': {
                    'token': orderDetail.token,
                    'id': orderDetail.user_id
                },
                'request': {
                    "orderid": orderDetail.order_id,
                    "lat": orderDetail.lat,
                    "lng": orderDetail.lng,
                    "radius": orderDetail.radius
                },
            };
            orderDetail.radius += 5;
            let timer = setTimeout(function() {
                obj.newOrderNotification(orderDetail);
            }, 30000);
            request.post({
                url: config.api,
                body: JSON.stringify(data)
            }, function(error, response, body){
                console.log(body);
                console.log(error);
                try {
                    body = JSON.parse(body);
                    if (body.success === 0 && body.code === 1100) {
                        if(timer){
                            clearTimeout(timer);
                            timer = null;
                        }
                    }
                } catch (e) {
                    console.error(e);
                }
            });
        }
    }
}

module.exports = new OrderController();