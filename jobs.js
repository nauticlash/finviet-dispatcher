'use strict';
var express = require('express'),
    order = require('./controller/order')
;
var app = express();
app.set('port', process.env.port || 7000);
var http = require('http').Server(app);
http.listen(app.get('port'), function(){
    // order.runWithFormat('*/1 * * * *', order.pendingOrders, {});
    // order.runWithFormat('*/1 * * * *', order.pendingDropoffs, {});
    // order.runWithFormat('*/1 * * * *', order.pendingPickups, {});
});
module.exports = app;