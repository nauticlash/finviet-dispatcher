'use strict';
let mysql = require('mysql');
let moment = require('moment');
let env = process.env.ENV || 'dev';
let config = require('../config/' + env + '/config');

let connection = mysql.createConnection({
    user: config.mysql.user,
    password: config.mysql.pass,
    host: config.mysql.host,
    port: config.mysql.port,
    database: config.mysql.database
});
connection.connect((err) => {
    if(err){
        console.log('Error connecting to Mysql');
        console.log(err);
        return;
    }
    console.log('Successfully connecting to Mysql');
});

class MysqlConnector {
    getExpiredPendingOrder(settings) {
        return new Promise((resolve, reject) => {
            let current = moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss');
            console.log(current);
            if (settings.order_pending_time && settings.order_pending_time > 0 && settings.order_pending_retry && settings.order_pending_retry > 0) {
                let query = "SELECT o.id FROM order_master o WHERE riderstatus = 'Pending' AND rider_id = 0 AND '" + current + "' > DATE_ADD(orderdate, INTERVAL "+ settings.order_pending_time + " * (alert_pending + 1) MINUTE) AND alert_pending < " + settings.order_pending_retry;
                console.log(query);
                connection.query(query, (err, res) => {
                    if(err) {
                        resolve([]);
                        throw err;
                    }
                    resolve(res);
                });
            } else {
                resolve([]);
            }
        });
    }

    getOverExpiredPendingOrder(settings) {
        return new Promise((resolve, reject) => {
            let current = moment().zone("07:00").format('YYYY-MM-DD HH:mm:ss');
            console.log(current);
            if (settings.order_pending_time && settings.order_pending_time > 0 && settings.order_pending_retry && settings.order_pending_retry > 0) {
                let query = "SELECT o.id FROM order_master o WHERE riderstatus = 'Pending' AND rider_id = 0 AND '" + current + "' > DATE_ADD(orderdate, INTERVAL "+ settings.order_pending_time * settings.order_pending_retry + " MINUTE) AND alert_pending >= " + settings.order_pending_retry;
                console.log(query);
                connection.query(query, (err, res) => {
                    if(err) {
                        resolve([]);
                        throw err;
                    }
                    resolve(res);
                });
            } else {
                resolve([]);
            }
        });
    }

    getExpiredPendingPickup(settings) {
        return new Promise((resolve, reject) => {
            let current = moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss');
            if (settings.rider_pickup_velocity && settings.rider_pickup_velocity > 0) {
                let distance = 5;
                let time = Math.round((distance / settings.rider_pickup_velocity) * 60);
                let query = "SELECT o.id FROM order_master o WHERE is_pickup < 3 AND rider_id != 0 AND '" + current + "' > DATE_ADD(orderdate, INTERVAL "+ time + " MINUTE) AND alert_pickup = 0";
                console.log(query);
                connection.query(query, (err, res) => {
                    if(err) {
                        resolve([]);
                        throw err;
                    }
                    resolve(res);
                });
            } else {
                resolve([]);
            }
        });
    }

    getExpiredPendingDropoff(settings) {
        return new Promise((resolve, reject) => {
            let current = moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss');
            if (settings.rider_dropoff_velocity && settings.rider_dropoff_velocity > 0) {
                let distance = 5;
                let time = Math.round((distance / settings.rider_dropoff_velocity) * 60);
                let query = "SELECT o.id FROM order_master o WHERE is_pickup = 3 AND rider_id != 0 AND is_dropoff != 2 AND '" + current + "' > DATE_ADD(orderdate, INTERVAL "+ time + " MINUTE) AND alert_dropoff = 0";
                console.log(query);
                connection.query(query, (err, res) => {
                    if(err) {
                        resolve([]);
                        throw err;
                    }
                    resolve(res);
                });
            } else {
                resolve([]);
            }
        });
    }

    getAlertSettings() {
        return new Promise((resolve, reject) => {
            let query = "SELECT order_pending_time, order_pending_retry, alert_email_notification, alert_email_list FROM setting_master";
            console.log(query);
            connection.query(query, (err, res) => {
                if(err) {
                    resolve(null);
                    throw err;
                }
                resolve(res[0]);
            });
        });
    }

    setAlertPending(ids) {
        if (ids.length > 0) {
            let current = moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss');
            let query = "UPDATE order_master SET alert_pending = alert_pending + 1, last_alert_pending = '" + current + "' WHERE id IN (" + ids + ")";
            console.log(query);
            connection.query(query, (err, res) => {
                if(err) {
                    throw err;
                }
            });
        }
    }

    setAlertPickup(ids) {
        if (ids.length > 0) {
            let current = moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss');
            let query = "UPDATE order_master SET alert_pickup = 1, last_alert_pickup = '" + current + "' WHERE id IN (" + ids + ")";
            console.log(query);
            connection.query(query, (err, res) => {
                if(err) {
                    throw err;
                }
            });
        }
    }

    setAlertDropoff(ids) {
        if (ids.length > 0) {
            let current = moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss');
            let query = "UPDATE order_master SET alert_dropoff = 1, last_alert_dropoff = '" + current + "' WHERE id IN (" + ids + ")";
            console.log(query);
            connection.query(query, (err, res) => {
                if(err) {
                    throw err;
                }
            });
        }
    }

    saveLocation(data) {
        if (data.user_id && data.latitude && data.longitude && data.timestamp) {
            let update_time = moment(data.timestamp).utcOffset(7).format("YYYY-MM-DD HH:mm:ss");
            let query = "UPDATE user_master SET lat = " + data.latitude + ", lng = " + data.longitude + ", modify_date = '" + update_time + "', last_location_updated = '" + update_time + "' WHERE id = " + data.user_id;
            connection.query(query, (err, res) => {
                if(err) {
                    console.log(err);
                }
            });
        }
    }

    getNearRiders(lat, lng, radius) {
        return new Promise((resolve, reject) => {
            let query = "SELECT id, deviceid, (SELECT COUNT(id) FROM order_master WHERE rider_id = u.id AND `riderstatus` NOT IN ('Complete','PickupDone')) AS freerider,(3959 * acos(cos(radians(" + lat + ")) * cos(radians(lat)) * cos( radians(lng) - radians(" + lng + ")) + sin(radians(" + lat + ")) * sin(radians(lat)))) AS distance FROM user_master as u WHERE usertype='driver' AND online_status='online' AND is_verify='1' AND '" + moment().utcOffset(7).format('YYYY-MM-DD HH:mm:ss') + "' < DATE_ADD( u.modify_date, INTERVAL 20 SECOND)  HAVING (distance < " + radius + " AND freerider = 0) ORDER BY `distance` ASC";
            console.log(query);
            connection.query(query, (err, res) => {
                if(err) {
                    resolve([]);
                    throw err;
                }
                resolve(res);
            });
        });
    }
}

module.exports = new MysqlConnector();