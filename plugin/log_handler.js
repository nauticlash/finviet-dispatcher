/**
 * Created by nauticlash on 8/24/19.
 */
const env = process.env.ENV || 'dev';
const config = require('../config/'+env+'/config');
const logger = require('fluent-logger');

logger.configure('dispatcher', {
    host: config.logger.host,
    port: config.logger.port,
    timeout: config.logger.timeout,
    reconnectInterval: config.logger.reconnectInterval // 10 minutes
});
exports.log = function(title, header, body, response) {
    console.log(title, header, body, response);
    let data = {
        'request_type': title,
        'header': header,
        'body': body,
        'response': response
    };
    logger.emit(title, data);
};
