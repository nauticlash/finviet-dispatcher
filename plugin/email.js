var env = process.env.ENV || 'dev';
var config = require('../config/'+env+'/config');
var nodemailer = require('nodemailer');

exports.send = function(data, callback) {
  // create reusable transporter object using the default SMTP transport
  var email = data.to;
  var transporter = nodemailer.createTransport({
    host: config.email.host,
    port: config.email.port,
    secure: true, // upgrade later with STARTTLS
    auth: {
        user: config.email.user,
        pass: config.email.pass
    },
    tls: {
        rejectUnauthorized: false
    }
  });

  // setup email data with unicode symbols
  var mailOptions = {
      from: '"Finviet System" <' + config.email.user + '>', // sender address
      to: data.to, // list of receivers
      subject: data.subject, // Subject line
      html: data.message // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
      }
      callback(info);
  });
};