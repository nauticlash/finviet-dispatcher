/**
 * Created by nauticlash on 8/24/19.
 */
const jwt   = require('jsonwebtoken');

exports.verify = function(token, callback) {
    try{
        callback(jwt.verify(token, 'bungkusit'));
    }catch (err){
        callback(false);
    }
};