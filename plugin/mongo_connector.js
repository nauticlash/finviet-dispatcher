'use strict';

const mongoose = require('mongoose');
let env = process.env.ENV || 'dev';
let config = require('../config/' + env + '/config');
let moment = require('moment');

let schema = new mongoose.Schema({
    user_id: String,
    latitude: String,
    longitude: String,
    speed: Number,
    heading: Number,
    timestamp: Number,
    order_id: String,
    unix_created_time: {
        type: Number,
        default: moment().valueOf()
    },
});
let Schema = mongoose.model('location', schema);

class MongoConnector {
    constructor() {
        this._connect()
    }
    _connect() {
        mongoose.connect(config.mongo.host)
            .then(() => {
                console.log('Connect to Mongo successfully')
            })
            .catch(err => {
                console.error('Connect to Mongo failed')
            })
    }

    saveLocation(data) {
        let location = new Schema({
            'user_id': data.user_id,
            'latitude': data.latitude,
            'longitude': data.longitude,
            'heading': data.heading,
            'speed': data.speed,
            'timestamp': data.timestamp,
            'order_id': data.order_id ? data.order_id : '',
            unix_created_time: moment().utcOffset(7).valueOf()
        });
        location.save(function (err, item) {
            if (err) return console.error(err);
        });
    }
}
module.exports = new MongoConnector();