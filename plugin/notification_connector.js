'use strict'
let FCM = require('fcm-node');
let env = process.env.ENV || 'dev';
let config = require('../config/' + env + '/config');
let fcm;

class NotificationConnector {
    constructor() {
        fcm = new FCM(config.fcm.serverKey);
    }

    newOrderNotification(to, orderDetail) {
        let message = "đã đặt đơn hàng mới. Vui lòng kiểm tra và nhận đơn. Mã đơn hàng: " + orderDetail.merchant_order_id;
        let content = {
            registration_ids: to,
            notification: {
                title: 'ECO Delivery',
                body: message
            },
            data: {
                message: message,
                mtype: 'neworder'
            }
        };

        fcm.send(content, function(err, response){
            if (err) {
                console.log("Something has gone wrong!");
                console.log(err);
            } else {
                console.log("Successfully sent with response: ", response);
            }
        });
    }
}
module.exports = new NotificationConnector();