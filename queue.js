'use strict';
const express = require('express'),
    redis = require("redis"),
    order = require('./controller/order')
;
let app = express(),
    env = process.env.ENV || 'dev',
    config = require('./config/' + env + '/config')
;
app.set('port', process.env.PORT || 3544);
app.use(function (err, req, res, next) {
    console.error(err.stack)
    res.status(500).send('Something broke!')
});
let http = require('http').Server(app);
http.listen(app.get('port'), function(){
    console.log('Node app is running on port', app.get('port'));
    let sub = create_redis_client(config.queue.host, config.queue.port);
    sub.on("message", function(channel, message) {
        message = JSON.parse(message);
        console.log("Message on channel " + channel + " data - " + JSON.stringify(message));
        switch (channel) {
            case config.queue.channel.new_order_notification:
                order.newOrderNotification(message);
                break;
            default:
                break;
        }
    });
    sub.subscribe(config.queue.channel.new_order_notification);
});

let create_redis_client = function create_redis_client(host, port){
    return redis.createClient({host: host, port: port});
}


module.exports = app;
