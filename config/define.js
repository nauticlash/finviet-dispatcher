var define = {
    cache: {
        driverOnline: 'mcar_driver_online',
        passengerOnline: 'mcar_passenger_online',
        geoLocation: 'mcar_geo',
        driverInfo: 'mcar_driver_info',
        passengerInfo: 'mcar_passenger_info',
        bookingInfo: 'mcar_booking_info',
        refIdInfo: 'mcar_ref_id_info',
        driver_available: 'driver_available',
        passenger_available: 'passenger_available',
        bookingId: 'mcar_booking_id',
    },
    api: {
        //url: 'http://unicar.taxionline.com.vn/',
        url: 'http://localhost:7979/api/',
    },
    unipay: {
        api: 'http://unipayapi.taxionline.com.vn/api/requestpayment',
        merchant_id: 2,
        service_id: 1,
    },
    socket_channel: {
        passsenger_save_booking: 'passengerSaveBooking',
        passsenger_save_booking_result: 'passengerSaveBookingResult',
        passsenger_cancel_trip: 'passengerCancelTrip',
        passsenger_cancel_trip_result: 'passengerCancelTripResult',
    },
    map_radius: 1000, // meter
    booking_timeout: 5000, // milisecond
    google_api_key: 'AIzaSyDNX-8WUlPoBc_NPWFqCXqQ3t4ZZzKgEb4',
    base_fare: 10000, // vnd per kilometer
    jwt: {
        driver_secret: "a5338238d10c64f761f8cfac52d133e5e3a5179704c9cdf8ffe78dafd490ee42",
        passenger_secret: "Rn4dPhtfa0svKuIWEtuBBKQ132F4s0yA8iMzUSBE1VxP2EK03BQESW910PWI7uRL",
        expires: "1d",
        timeout: 5000,
        algorithm: "HS256"
    }
};
module.exports = define;
