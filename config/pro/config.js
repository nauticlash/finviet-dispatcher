var config = {
    redis: {
        host: '127.0.0.1', // The redis's server ip
        port: '6379',
        location: {
            prefix: 'bungkusit_pos_',
            expired_time: 10
        }
    },
    queue: {
        host: '127.0.0.1', // The redis's queue ip
        port: '6379',
        channel: {
            new_order_notification: 'new_order_notification'
        }
    },
    mysql: {
        host: '127.0.0.1',
        port: 3306,
        user: 'finviet',
        pass: 'DZm?w[sO?;%f',
        database: 'finviet_takeit'
    },
    email: {
        user: 'finvietdeveloper@gmail.com',
        pass: 'Finviet2019',
        host: 'smtp.gmail.com',
        port: 465
    },
    mongo: {
        host: 'mongodb://127.0.0.1/finviet',
        port: '27017',
    },
    api: 'http://finviet-api.7tech.ai/webService/service',
    fcm: {
        serverKey: 'AAAA-uuJo6U:APA91bHLdAhYa8tcCIXBYs8NcP8ieeSXYX6LLfrHZH4YpzxDTxy89VKroctplwnGTyLqVZY72mJUIxLZBk1XIbciQwhWhcPna-MO8-xed5Z02JZIzFoGfXOtOA_1hb4HvYt33K-A4TMN'
    },
    logger: {
        host: '127.0.0.1',
        port: 24224,
        timeout: 3,
        reconnectInterval: 600000
    },
};
module.exports = config;
